import React, {useEffect, useState} from 'react';
import {Breadcrumb, Button, ButtonGroup, Carousel, Form, FormControl, Nav, Navbar, NavDropdown} from "react-bootstrap";
import Gallery from "../components/Gallery";
import slider_data from "../data/slider.json";
import data from "../data/gallery.json";

export const Main = () => {

    const [movies_gallery, setMGallery] = useState([]);
    const [cartoons_gallery, setCGallery] = useState([]);
    const [series_gallery, setSGallery] = useState([]);

    const [feedback, setFeedback] = useState("");
    const [disabled, setDisabled] = useState(false);

    const [s_data, setData] = useState([]);

    const fetchGallery = () => {
        const movie_image = data.movie_images;
        setMGallery(movie_image);
        const cartoon_image = data.cartoon_images;
        setCGallery(cartoon_image);
        const series_image = data.series_images;
        setSGallery(series_image);
        const sl = slider_data.home;
        setData(sl);
    }

    useEffect(() => {
        fetchGallery();
        setFeedback("Did you enjoy this website?")
    }, [])

    const onClickFeedback = (e) => {
        e.preventDefault();
        setFeedback("Thank you for the feedback")
        setDisabled(true);
    }


    return (
        <body>
        <header>
            <Carousel fade className="slider">
                {s_data.map((d)=>
                    <Carousel.Item className="slider_item">
                        <img
                            src={d.src}
                            alt="Slide"
                        />
                        <Carousel.Caption className="slider_caption_back">
                            <h3>{d.name}</h3>
                            <p>{d.year}</p>
                        </Carousel.Caption>
                    </Carousel.Item>

                )}
            </Carousel>
            <Navbar bg="dark" variant="dark" fixed="top" className="w-60%">
                <Navbar.Toggle aria-controls="navbarScroll"/>
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="mr-auto my-2 my-lg-0"
                        style={{maxHeight: '100px'}}
                        navbarScroll
                    >
                        <Nav.Link href="/" disabled>Home</Nav.Link>
                        <div className="vertical_line"></div>
                        <NavDropdown title="Cinematography" id="navbarScrollingDropdown">
                            <NavDropdown.Item href="/topmovies">Top</NavDropdown.Item>
                            <NavDropdown.Item href="/moviestat">Statistics</NavDropdown.Item>
                            <NavDropdown.Divider/>
                            <NavDropdown.Item href="/movies">All</NavDropdown.Item>
                        </NavDropdown>
                        <div className="vertical_line"></div>
                        <NavDropdown title="Actors" id="navbarScrollingDropdown">
                            <NavDropdown.Item href="/topactors">Top</NavDropdown.Item>
                            <NavDropdown.Item href="/actorstat">Statistics</NavDropdown.Item>
                            <NavDropdown.Divider/>
                            <NavDropdown.Item href="/actors">All</NavDropdown.Item>
                        </NavDropdown>
                        <div className="vertical_line"></div>
                        <Nav.Link href="/contacts">Contacts</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </header>
        <main>
            <Breadcrumb>
                <Breadcrumb.Item href="/" active="false">Home</Breadcrumb.Item>
            </Breadcrumb>
            <div className="horizontal_line"></div>
            <p align="center" className="head_line">Movies</p>
            <Gallery
                show={3}
            >

                {
                    movies_gallery.map((image) =>
                        <div>
                            <div style={{padding: 8}} className="gallery_image_parent">
                                <img
                                    src={image.src}
                                    alt="gallery_image"/>
                            </div>
                        </div>
                    )
                }
            </Gallery>
            <div className="horizontal_line"></div>
            <p align="center" className="head_line">Cartoons</p>
            <Gallery
                show={3}
            >

                {
                    cartoons_gallery.map((image) =>
                        <div>
                            <div style={{padding: 8}} className="gallery_image_parent">
                                <img
                                    src={image.src}
                                    alt="gallery_image"/>
                            </div>
                        </div>
                    )
                }
            </Gallery>
            <div className="horizontal_line"></div>
            <p align="center" className="head_line">Series</p>
            <Gallery
                show={3}
            >

                {
                    series_gallery.map((image) =>
                        <div>
                            <div style={{padding: 8}} className="gallery_image_parent">
                                <img
                                    src={image.src}
                                    alt="gallery_image"/>
                            </div>
                        </div>
                    )
                }
            </Gallery>
            <div className="horizontal_line"></div>

        </main>
        <footer>
            <p className="footer_question">{feedback}</p>
            <ButtonGroup aria-label="Basic example">
                <Button variant="secondary" className="footer_button" disabled={disabled}
                        onClick={(e) => onClickFeedback(e)}
                >Yes</Button>
                <Button variant="secondary" className="footer_button"
                        onClick={(e) => onClickFeedback(e)} disabled={disabled}
                >No</Button>
            </ButtonGroup>
            <p></p>
        </footer>
        </body>
    );
}
