import React, {useEffect, useState} from 'react';
import slider_data from "../data/slider.json";
import {Breadcrumb, Button, ButtonGroup, Carousel, Form, FormControl, Nav, Navbar, NavDropdown} from "react-bootstrap";
import Map from "../components/Map";

export const Contacts = (props) => {
    const [s_data, setData] = useState([]);
    const fetchData = () => {
        const sl = slider_data.contacts;
        setData(sl);
    }

    const [feedback, setFeedback] = useState("");
    const [disabled, setDisabled] = useState(false);

    const [position, setPosition] = useState([]);

    const onClickFeedback = (e) => {
        e.preventDefault();
        setFeedback("Thank you for the feedback")
        setDisabled(true);
    }

    useEffect(() => {
        setFeedback("Did you enjoy this website?")
        setPosition([51.505, -0.09])
        fetchData();
    }, [])
    return (
        <>
            <header>
                <Carousel fade className="slider">
                    {s_data.map((d) =>
                        <Carousel.Item className="slider_item">
                            <img
                                src={d.src}
                                alt="Slide"
                            />
                            <Carousel.Caption className="slider_caption_back">
                                <h3>{d.name}</h3>
                                <p>{d.year}</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )}
                </Carousel>
                <Navbar bg="dark" variant="dark" fixed="top" className="w-60%">
                    <Navbar.Toggle aria-controls="navbarScroll"/>
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="mr-auto my-2 my-lg-0"
                            style={{maxHeight: '100px'}}
                            navbarScroll
                        >
                            <Nav.Link href="/">Home</Nav.Link>
                            <div className="vertical_line"></div>
                            <NavDropdown title="Cinematography" id="navbarScrollingDropdown">
                                <NavDropdown.Item href="/topmovies">Top</NavDropdown.Item>
                                <NavDropdown.Item href="/moviestat">Statistics</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item href="/movies">All</NavDropdown.Item>
                            </NavDropdown>
                            <div className="vertical_line"></div>
                            <NavDropdown title="Actors" id="navbarScrollingDropdown">
                                <NavDropdown.Item href="/topactors">Top</NavDropdown.Item>
                                <NavDropdown.Item href="/actorstat">Statistics</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item href="/actors">All</NavDropdown.Item>
                            </NavDropdown>
                            <div className="vertical_line"></div>
                            <Nav.Link href="/contacts" disabled>Contacts</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </header>
            <main>
                <Breadcrumb>
                    <Breadcrumb.Item href="/contacts" active="false">Contacts</Breadcrumb.Item>
                </Breadcrumb>
                <div className="horizontal_line"></div>
                <p align="center" className="head_line">Contacts</p>
                <div style={{ margin:"0px 0px 550px 0px"}}>
                    <Map/>
                </div>
                <div className="horizontal_line"></div>


            </main>
            <footer>
                <p className="footer_question">{feedback}</p>
                <ButtonGroup aria-label="Basic example">
                    <Button variant="secondary" className="footer_button" disabled={disabled}
                            onClick={(e) => onClickFeedback(e)}
                    >Yes</Button>
                    <Button variant="secondary" className="footer_button"
                            onClick={(e) => onClickFeedback(e)} disabled={disabled}
                    >No</Button>
                </ButtonGroup>
                <p></p>
            </footer>
        </>
    );
};

