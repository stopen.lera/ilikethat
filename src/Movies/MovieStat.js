import React, {useEffect, useState} from 'react';
import {Bar} from 'react-chartjs-2';
import {Breadcrumb, Button, ButtonGroup, Carousel, Form, FormControl, Nav, Navbar, NavDropdown} from "react-bootstrap";
import data from "../data/movies.json";
import slider_data from "../data/slider.json";
export const MovieStat = () => {
    const [s_data, setData] = useState([]);
    const fetchData = () => {
        const sl = slider_data.movie_stats;
        setData(sl);
    }

    const [feedback, setFeedback] = useState("");
    const [disabled, setDisabled] = useState(false);

    const onClickFeedback = (e) => {
        e.preventDefault();
        setFeedback("Thank you for the feedback")
        setDisabled(true);
    }

    const [chartData, setCharData] = useState();
    const fetchCharData = ()=>{
        data.movies.sort(function (a, b) {
            if (a.year > b.year) {return 1;}
            if (a.year < b.year) {return -1;}
            return 0;
        });
        let label = [];
        data.movies.map((m)=>{
            if(!label.includes(m.year)){
                label.push(m.year);
            }
        })
        let d = [];
        label.map((m)=>{
            let c = 0;
            data.movies.map((m_2)=>{
                if(m_2.year === m)
                {
                    c++
                }
            })
            d.push(c);
        })
        console.log(label);
        setCharData({
            labels: label,
            datasets: [
                {
                    label: 'Movies in Year',
                    data: d,
                    backgroundColor: [
                        'rgba(32, 38, 33, 0.6)',
                        'rgba(51, 190, 231, 0.6)',
                    ],
                    borderWidth: 1
                }
            ]
        })
    }
    useEffect(() => {
        fetchData();
        fetchCharData();
        setFeedback("Did you enjoy this chart?")
    },[])
    const [barOptions, setBarOptions] = useState({
        options: {
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            },
            title: {
                display: true,
                text: 'Data Orgranized In Bars',
                fontSize: 25
            },
            legend: {
                display: true,
                position: 'top'
            }
        }
    });
    return (
        <>
            <header>
                <Carousel fade className="slider">
                    {s_data.map((d) =>
                        <Carousel.Item className="slider_item">
                            <img
                                src={d.src}
                                alt="Slide"
                            />
                            <Carousel.Caption className="slider_caption_back">
                                <h3>{d.name}</h3>
                                <p>{d.year}</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )}
                </Carousel>
                <Navbar bg="dark" variant="dark" fixed="top" className="w-60%">
                    <Navbar.Toggle aria-controls="navbarScroll"/>
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="mr-auto my-2 my-lg-0"
                            style={{maxHeight: '100px'}}
                            navbarScroll
                        >
                            <Nav.Link href="/">Home</Nav.Link>
                            <div className="vertical_line"></div>
                            <NavDropdown title="Cinematography" id="navbarScrollingDropdown">
                                <NavDropdown.Item href="/topmovies">Top</NavDropdown.Item>
                                <NavDropdown.Item href="/moviestat" disabled>Statistics</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item href="/movies" >All</NavDropdown.Item>
                            </NavDropdown>
                            <div className="vertical_line"></div>
                            <NavDropdown title="Actors" id="navbarScrollingDropdown">
                                <NavDropdown.Item href="/topactors">Top</NavDropdown.Item>
                                <NavDropdown.Item href="/actorstat">Statistics</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item href="/actors">All</NavDropdown.Item>
                            </NavDropdown>
                            <div className="vertical_line"></div>
                            <Nav.Link href="/contacts">Contacts</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </header>
            <main>
                <Breadcrumb>
                    <Breadcrumb.Item href="/movies">Movies</Breadcrumb.Item>
                    <Breadcrumb.Item href="/moviestat" active="false">Movie Statistics</Breadcrumb.Item>
                </Breadcrumb>
                <div className="horizontal_line"></div>
                <p align="center" className="head_line">Statistics</p>
                <div className="BarExample">
                    <Bar
                        data={chartData}
                        options={barOptions.options} />
                </div>
                <div className="horizontal_line"></div>
            </main>
            <footer>
                <p className="footer_question">{feedback}</p>
                <ButtonGroup aria-label="Basic example">
                    <Button variant="secondary" className="footer_button" disabled={disabled}
                            onClick={(e) => onClickFeedback(e)}
                    >Yes</Button>
                    <Button variant="secondary" className="footer_button"
                            onClick={(e) => onClickFeedback(e)} disabled={disabled}
                    >No</Button>
                </ButtonGroup>
                <p></p>
            </footer>

        </>
    );
};
