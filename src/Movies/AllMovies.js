import React, {useEffect, useState} from 'react';
import {
    Breadcrumb,
    Button,
    ButtonGroup,
    Carousel,
    Dropdown,
    DropdownButton,
    Form,
    FormControl,
    Nav,
    Navbar,
    NavDropdown,
    Pagination
} from "react-bootstrap";
import {Box} from "@material-ui/core";
import {Rating} from "@material-ui/lab";
import data from "../data/movies.json";
import Service from "../Service/Service";
import slider_data from "../data/slider.json";

export const AllMovies = () => {
        const [s_data, setData] = useState([]);
        const fetchData = () => {
            const sl = slider_data.movies;
            setData(sl);
        }

        const [movies, setMovies] = useState([]);
        const [genres, setGenres] = useState([]);

        const [filter, setFilter] = useState("all");
        const [sort, setSort] = useState("default");

        const [feedback, setFeedback] = useState("");
        const [disabled, setDisabled] = useState(false);

        const [search, setSearch] = useState("");

        const [current_page, setCurrentPage] = useState(0);
        const [html_pages, setHtmlPages] = useState([]);

        const createPages = (total) => {
            setHtmlPages([]);
            for (let i = 0; i < total; i++) {
                setHtmlPages(prev => [...prev,
                    <Pagination.Item
                        key={i}
                        active={i === current_page}
                        onClick={() => {
                            if (i !== current_page) {
                                fetchMovies(data.movies, filter, sort, search, i);
                                setCurrentPage(i);
                            }
                        }}>
                        {i + 1}
                    </Pagination.Item>])
            }
        }

        const onClickFeedback = (e) => {
            e.preventDefault();
            setFeedback("Thank you for the feedback")
            setDisabled(true);
        }

        const fetchMovies = (data, param, sort, search, currentPage) => {
            Service.fetchMoviesChanged(data, param, sort, search, currentPage).then(resp => {
                setMovies(resp[0]);
                createPages(Math.ceil(resp[1] / 5));
            }).catch(err => {alert(err); window.location.reload()})
        }

        const fetchGenres = () => {
            const movie = data.movies;
            let genre = [];
            for (let i = 0; i < movie.length; i++) {
                genre.push(movie[i].genre);
            }
            setGenres(Array.from(new Set(genre)));
        }
        const handleSearchChange = (e) => {
            setSearch(e.target.value);
        }


        useEffect(() => {
            setFeedback("Did you enjoy this list?")
            fetchMovies(data.movies, 'all', "default", "", current_page);
            fetchGenres();
            fetchData();
        }, [])

        useEffect(() => {
            fetchMovies(data.movies, filter, sort, search, current_page)
        }, [current_page])

        useEffect(() => {
            fetchMovies(data.movies, filter, sort, search, 0)
        }, [filter])

        useEffect(() => {
            fetchMovies(data.movies, filter, sort, search, 0)
        }, [sort])

        useEffect(() => {
            fetchMovies(data.movies, filter, sort, search, 0)
        }, [search])


        return (<>
                <header>
                    <Carousel fade className="slider">
                        {s_data.map((d) =>
                            <Carousel.Item className="slider_item">
                                <img
                                    src={d.src}
                                    alt="Slide"
                                />
                                <Carousel.Caption className="slider_caption_back">
                                    <h3>{d.name}</h3>
                                    <p>{d.year}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )}
                    </Carousel>
                    <Navbar bg="dark" variant="dark" fixed="top" className="w-60%">
                        <Navbar.Toggle aria-controls="navbarScroll"/>
                        <Navbar.Collapse id="navbarScroll">
                            <Nav
                                className="mr-auto my-2 my-lg-0"
                                style={{maxHeight: '100px'}}
                                navbarScroll
                            >
                                <Nav.Link href="/">Home</Nav.Link>
                                <div className="vertical_line"></div>
                                <NavDropdown title="Cinematography" id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="/topmovies">Top</NavDropdown.Item>
                                    <NavDropdown.Item href="/moviestat">Statistics</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item href="/movies" disabled>All</NavDropdown.Item>
                                </NavDropdown>
                                <div className="vertical_line"></div>
                                <NavDropdown title="Actors" id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="/topactors">Top</NavDropdown.Item>
                                    <NavDropdown.Item href="/actorstat">Statistics</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item href="/actors">All</NavDropdown.Item>
                                </NavDropdown>
                                <div className="vertical_line"></div>
                                <Nav.Link href="/contacts">Contacts</Nav.Link>
                            </Nav>
                            <Form className="d-flex">
                                <FormControl
                                    type="search"
                                    placeholder="Search for movies"
                                    className="mr-2"
                                    aria-label="Search"
                                    onChange={(e) => handleSearchChange(e)}
                                />
                            </Form>
                        </Navbar.Collapse>
                    </Navbar>
                </header>
                <main>
                    <Breadcrumb>
                        <Breadcrumb.Item href="/movies" active="false">Movies</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="horizontal_line"></div>
                    <p align="center" className="head_line">Movies</p>
                    <div className="filter_sort">
                        <DropdownButton
                            alignRight
                            title="Genre"
                            className="movie_filter"
                            variant="secondary"
                            onSelect={(e) => {
                                setFilter(e)
                            }}
                        >
                            <Dropdown.Item eventKey="all">all</Dropdown.Item>
                            {
                                genres.map((genre) =>

                                    <Dropdown.Item eventKey={genre}>{genre}</Dropdown.Item>)

                            }
                        </DropdownButton>
                        <p>Filter: {filter}</p>
                        <DropdownButton
                            alignRight
                            title="Year"
                            className="movie_filter"
                            variant="secondary"
                            onSelect={(e) => {
                                setSort(e)
                            }}
                        >
                            <Dropdown.Item eventKey="default">default</Dropdown.Item>
                            <Dropdown.Item eventKey="from oldest to newest">sort from oldest to newest</Dropdown.Item>
                            <Dropdown.Item eventKey="from newest to oldest">sort from newest to oldest</Dropdown.Item>
                        </DropdownButton>
                        <p>Sort: {sort}</p>
                    </div>
                    {
                        movies.map((movie) =>

                            <div className="card">
                                <div className="card_horizontal">
                                    <div className="card_image_parent">
                                        <img src={movie.src} alt="Card image cap"/>
                                    </div>
                                    <div className="card-body">
                                        <h4>{movie.name}</h4>
                                        <p>{movie.year}</p>
                                        <p>{movie.genre}</p>
                                        <Box component="fieldset" mb={3} borderColor="transparent">
                                            <Rating name="read-only" className="movie_rating" value={movie.rate} readOnly/>
                                        </Box>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    <Pagination className="pagination">
                        {html_pages}
                    </Pagination>
                    <div className="horizontal_line"></div>
                </main>
                <footer>
                    <p className="footer_question">{feedback}</p>
                    <ButtonGroup aria-label="Basic example">
                        <Button variant="secondary" className="footer_button" disabled={disabled}
                                onClick={(e) => onClickFeedback(e)}
                        >Yes</Button>
                        <Button variant="secondary" className="footer_button"
                                onClick={(e) => onClickFeedback(e)} disabled={disabled}
                        >No</Button>
                    </ButtonGroup>
                    <p></p>
                </footer>
            </>
        );
    }
;
