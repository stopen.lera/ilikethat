import React, {useEffect, useState} from 'react';
import {
    Breadcrumb,
    Button,
    ButtonGroup,
    Carousel,
    Dropdown,
    DropdownButton,
    Form,
    FormControl,
    Nav,
    Navbar,
    NavDropdown,
    Pagination
} from "react-bootstrap";
import {Box} from "@material-ui/core";
import {Rating} from "@material-ui/lab";
import data from "../data/movies.json";
import Service from "../Service/Service";
import slider_data from "../data/slider.json";

export const TopMovies = () => {
        const [s_data, setData] = useState([]);
        const fetchData = () => {
            const sl = slider_data.top_movies;
            setData(sl);
        }

        const [movies, setMovies] = useState([]);
        const [types, setTypes] = useState([]);

        const [filter, setFilter] = useState("all");

        const [feedback, setFeedback] = useState("");
        const [disabled, setDisabled] = useState(false);

        const [current_page, setCurrentPage] = useState(0);
        const [html_pages, setHtmlPages] = useState([]);


        const createPages = (total) => {
            setHtmlPages([]);
            for (let i = 0; i < total; i++) {
                setHtmlPages(prev => [...prev,
                    <Pagination.Item
                        key={i}
                        active={i === current_page}
                        onClick={() => {
                            if (i !== current_page) {
                                fetchTopMovies(data.movies, filter, i);
                                setCurrentPage(i);
                            }
                        }}>
                        {i + 1}
                    </Pagination.Item>])
            }
        }

        const onClickFeedback = (e) => {
            e.preventDefault();
            setFeedback("Thank you for the feedback")
            setDisabled(true);
        }

        const fetchTopMovies = (data, param, currentPage) => {
            Service.fetchTopMoviesChanged(data, param, currentPage).then(resp => {
                setMovies(resp[0]);
                createPages(Math.ceil(resp[1] / 5));
            }).catch(err => {alert(err); window.location.reload()})
        }

        const fetchTypes = () => {
            const movie = data.movies;
            let type = [];
            for (let i = 0; i < movie.length; i++) {
                type.push(movie[i].type);
            }
            setTypes(Array.from(new Set(type)));
        }


        useEffect(() => {
            setFeedback("Did you enjoy this list?")
            fetchTopMovies(data.movies, 'all', current_page);
            fetchTypes();
            fetchData();
        }, [])

        useEffect(() => {
            fetchTopMovies(data.movies, filter, current_page)
        }, [current_page])

        useEffect(() => {
            fetchTopMovies(data.movies, filter, 0)
        }, [filter])


        return (<>
                <header>
                    <Carousel fade className="slider">
                        {s_data.map((d) =>
                            <Carousel.Item className="slider_item">
                                <img
                                    src={d.src}
                                    alt="Slide"
                                />
                                <Carousel.Caption className="slider_caption_back">
                                    <h3>{d.name}</h3>
                                    <p>{d.year}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )}
                    </Carousel>
                    <Navbar bg="dark" variant="dark" fixed="top" className="w-60%">
                        <Navbar.Toggle aria-controls="navbarScroll"/>
                        <Navbar.Collapse id="navbarScroll">
                            <Nav
                                className="mr-auto my-2 my-lg-0"
                                style={{maxHeight: '100px'}}
                                navbarScroll
                            >
                                <Nav.Link href="/">Home</Nav.Link>
                                <div className="vertical_line"></div>
                                <NavDropdown title="Cinematography" id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="/topmovies" disabled>Top</NavDropdown.Item>
                                    <NavDropdown.Item href="/moviestat">Statistics</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item href="/movies">All</NavDropdown.Item>
                                </NavDropdown>
                                <div className="vertical_line"></div>
                                <NavDropdown title="Actors" id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="/topactors">Top</NavDropdown.Item>
                                    <NavDropdown.Item href="/actorstat">Statistics</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item href="/actors">All</NavDropdown.Item>
                                </NavDropdown>
                                <div className="vertical_line"></div>
                                <Nav.Link href="/contacts">Contacts</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </header>
                <main>
                    <Breadcrumb>
                        <Breadcrumb.Item href="/movies">Movies</Breadcrumb.Item>
                        <Breadcrumb.Item href="/topmovies" active="false">Top Movies</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="horizontal_line"></div>
                    <p align="center" className="head_line">Top</p>
                    <div className="filter_sort">
                        <DropdownButton
                            alignRight
                            title="Type"
                            className="movie_filter"
                            variant="secondary"
                            onSelect={(e) => {
                                setFilter(e)
                            }}
                        >
                            <Dropdown.Item eventKey="all">all</Dropdown.Item>
                            {
                                types.map((type) =>

                                    <Dropdown.Item eventKey={type}>{type}</Dropdown.Item>)

                            }
                        </DropdownButton>
                        <p>Filter: {filter}</p>
                    </div>
                    {
                        movies.map((movie) =>

                            <div className="card">
                                <div className="card_horizontal">
                                    <div className="card_image_parent">
                                        <img src={movie.src} alt="Card image cap"/>
                                    </div>
                                    <div className="card-body">
                                        <h4>{movie.name}</h4>
                                        <p>{movie.year}</p>
                                        <p>{movie.genre}</p>
                                        <Box component="fieldset" mb={3} borderColor="transparent">
                                            <Rating name="read-only" className="movie_rating" value={movie.rate} readOnly/>
                                        </Box>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    <Pagination className="pagination">
                        {html_pages}
                    </Pagination>
                    <div className="horizontal_line"></div>
                </main>
                <footer>
                    <p className="footer_question">{feedback}</p>
                    <ButtonGroup aria-label="Basic example">
                        <Button variant="secondary" className="footer_button" disabled={disabled}
                                onClick={(e) => onClickFeedback(e)}
                        >Yes</Button>
                        <Button variant="secondary" className="footer_button"
                                onClick={(e) => onClickFeedback(e)} disabled={disabled}
                        >No</Button>
                    </ButtonGroup>
                    <p></p>
                </footer>
            </>
        );
    }
;
