const moviesPerPage = 5;

const sliceMovies = (movies, currentPage) => {
    return movies.slice((currentPage) * moviesPerPage, (currentPage) * moviesPerPage + moviesPerPage);
}
const fetchMoviesChanged = (someData, filter, sort, search, currentPage) => {
    return new Promise(function (resolve, reject) {
        let movies = []
        someData.map((m) => {
            if (m.name.includes(search)) {
                movies.push(m);
            }
        })
        if (filter !== 'all') {
            movies= movies.filter(m => m.genre == filter)
        }
        if(sort !== 'default')
        {
            movies.sort(function (a, b) {
                if (a.year > b.year) {
                    if(sort==="from oldest to newest")
                    {
                        return 1;
                    }
                    return -1;
                }
                if (a.year < b.year) {
                    if(sort==="from oldest to newest")
                    {
                        return -1;
                    }
                    return 1;
                }
                return 0;
            });
        }
        if(movies.length==0)
        {
            reject(new Error("empty list"));
        }
        resolve([sliceMovies(movies, currentPage), movies.length]);
    });
}
const fetchActorsChanged = (someData, filter, sort, search, currentPage) => {
    return new Promise(function (resolve, reject) {
        let actors = []
        someData.map((m) => {
            if (m.name.includes(search)) {
                actors.push(m);
            }
        })
        if (filter !== 'all') {
            actors= actors.filter(a => a.country === filter)
        }
        if(sort !== 'default')
        {
            actors.sort(function (a, b) {
                if (a.age > b.age) {
                    if(sort==="from highest to lowest")
                    {
                        return -1;
                    }
                    return 1;
                }
                if (a.age < b.age) {
                    if(sort==="from highest to lowest")
                    {
                        return 1;
                    }
                    return -1;
                }
                return 0;
            });
        }
        if(actors.length==0)
        {
            reject(new Error("empty list"));
        }
        resolve([sliceMovies(actors, currentPage), actors.length]);
    });
}

const fetchTopMoviesChanged = (someData, filter, currentPage) => {
    return new Promise(function (resolve, reject) {
        let movies = []
        someData.map((m) => {
            if (m.rate === 5 && m.type===filter) {
                movies.push(m);
            }
        })
        if(filter === "all"){
            someData.map((m) => {
                if (m.rate === 5) {
                    movies.push(m);
                }
            })
        }
        if(movies.length==0)
        {
            reject(new Error("empty list"));
        }
        resolve([sliceMovies(movies, currentPage), movies.length]);
    });
}

const fetchTopActorsChanged = (someData, currentPage) => {
    return new Promise(function (resolve, reject) {
        let actors = []
        someData.map((m) => {
            if (m.rate === 5) {
                actors.push(m);
            }
        })
        if(actors.length==0)
        {
            reject(new Error("empty list"));
        }
        resolve([sliceMovies(actors, currentPage), actors.length]);
    });
}

export default {
    fetchMoviesChanged,
    fetchActorsChanged,
    fetchTopMoviesChanged,
    fetchTopActorsChanged
}
