import React, {useEffect, useState} from 'react';
import {
    Breadcrumb,
    Button,
    ButtonGroup,
    Carousel,
    Dropdown,
    DropdownButton,
    Form,
    FormControl,
    Nav,
    Navbar,
    NavDropdown,
    Pagination
} from "react-bootstrap";
import {Box} from "@material-ui/core";
import {Rating} from "@material-ui/lab";
import data from "../data/actors.json";
import Service from "../Service/Service";
import slider_data from "../data/slider.json";

export const AllActor = () => {
        const [s_data, setData] = useState([]);
        const fetchData = () => {
            const sl = slider_data.actors;
            setData(sl);
        }

        const [actors, setActors] = useState([]);
        const [countries, setCountries] = useState([]);

        const [filter, setFilter] = useState("all");
        const [sort, setSort] = useState("default");

        const [feedback, setFeedback] = useState("");
        const [disabled, setDisabled] = useState(false);

        const [search, setSearch] = useState("");

        const [current_page, setCurrentPage] = useState(0);
        const [html_pages, setHtmlPages] = useState([]);


        const createPages = (total) => {
            setHtmlPages([]);
            for (let i = 0; i < total; i++) {
                setHtmlPages(prev => [...prev,
                    <Pagination.Item
                        key={i}
                        active={i === current_page}
                        onClick={() => {
                            if (i !== current_page) {
                                fetchActors(data.actors, filter, sort, search, i);
                                setCurrentPage(i);
                            }
                        }}>
                        {i + 1}
                    </Pagination.Item>])
            }
        }

        const onClickFeedback = (e) => {
            e.preventDefault();
            setFeedback("Thank you for the feedback")
            setDisabled(true);
        }

        const fetchActors = (data, param, sort, search, currentPage) => {
            Service.fetchActorsChanged(data, param, sort, search, currentPage).then(resp => {
                setActors(resp[0]);
                createPages(Math.ceil(resp[1] / 5));
            }).catch(err => {alert(err); window.location.reload()})
        }

        const fetchCountries = () => {
            const actor = data.actors;
            let country = [];
            for (let i = 0; i < actor.length; i++) {
                country.push(actor[i].country);
            }
            setCountries(Array.from(new Set(country)));
        }
        const handleSearchChange = (e) => {
            setSearch(e.target.value);
        }


        useEffect(() => {
            setFeedback("Did you enjoy this list?")
            fetchActors(data.actors, 'all', "default", "", current_page);
            fetchCountries();
            fetchData();
        }, [])

        useEffect(() => {
            fetchActors(data.actors, filter, sort, search, current_page)
        }, [current_page])

        useEffect(() => {
            fetchActors(data.actors, filter, sort, search, 0)
        }, [filter])

        useEffect(() => {
            fetchActors(data.actors, filter, sort, search, 0)
        }, [sort])

        useEffect(() => {
            fetchActors(data.actors, filter, sort, search, 0)
        }, [search])


        return (<>
                <header>
                    <Carousel fade className="slider">
                        {s_data.map((d) =>
                            <Carousel.Item className="slider_item">
                                <img
                                    src={d.src}
                                    alt="Slide"
                                />
                                <Carousel.Caption className="slider_caption_back">
                                    <h3>{d.name}</h3>
                                    <p>{d.year}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )}
                    </Carousel>
                    <Navbar bg="dark" variant="dark" fixed="top" className="w-60%">
                        <Navbar.Toggle aria-controls="navbarScroll"/>
                        <Navbar.Collapse id="navbarScroll">
                            <Nav
                                className="mr-auto my-2 my-lg-0"
                                style={{maxHeight: '100px'}}
                                navbarScroll
                            >
                                <Nav.Link href="/">Home</Nav.Link>
                                <div className="vertical_line"></div>
                                <NavDropdown title="Cinematography" id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="/topmovies">Top</NavDropdown.Item>
                                    <NavDropdown.Item href="/moviestat">Statistics</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item href="/movies">All</NavDropdown.Item>
                                </NavDropdown>
                                <div className="vertical_line"></div>
                                <NavDropdown title="Actors" id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="/topactors">Top</NavDropdown.Item>
                                    <NavDropdown.Item href="/actorstat">Statistics</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item href="/actors" disabled>All</NavDropdown.Item>
                                </NavDropdown>
                                <div className="vertical_line"></div>
                                <Nav.Link href="/contacts">Contacts</Nav.Link>
                            </Nav>
                            <Form className="d-flex">
                                <FormControl
                                    type="search"
                                    placeholder="Search for actors"
                                    className="mr-2"
                                    aria-label="Search"
                                    onChange={(e) => handleSearchChange(e)}
                                />
                            </Form>
                        </Navbar.Collapse>
                    </Navbar>
                </header>
                <main>
                    <Breadcrumb>
                        <Breadcrumb.Item href="/actors" active="false">Actors</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="horizontal_line"></div>
                    <p align="center" className="head_line">Actors</p>
                    <div className="filter_sort">
                        <DropdownButton
                            alignRight
                            title="Country"
                            className="movie_filter"
                            variant="secondary"
                            onSelect={(e) => {
                                setFilter(e)
                            }}
                        >
                            <Dropdown.Item eventKey="all">all</Dropdown.Item>
                            {
                                countries.map((country) =>

                                    <Dropdown.Item eventKey={country}>{country}</Dropdown.Item>)

                            }
                        </DropdownButton>
                        <p>Filter: {filter}</p>
                        <DropdownButton
                            alignRight
                            title="Age"
                            className="movie_filter"
                            variant="secondary"
                            onSelect={(e) => {
                                setSort(e)
                            }}
                        >
                            <Dropdown.Item eventKey="default">default</Dropdown.Item>
                            <Dropdown.Item eventKey="from highest to lowest">sort from highest to lowest</Dropdown.Item>
                            <Dropdown.Item eventKey="from lowest to highest">sort from lowest to highest</Dropdown.Item>
                        </DropdownButton>
                        <p>Sort: {sort}</p>
                    </div>
                    {
                        actors.map((actor) =>

                            <div className="card">
                                <div className="card_horizontal">
                                    <div className="card_image_parent">
                                        <img src={actor.src} alt="Card image cap"/>
                                    </div>
                                    <div className="card-body">
                                        <h4>{actor.name}</h4>
                                        <p>{actor.age}</p>
                                        <p>{actor.country}</p>
                                        <Box component="fieldset" mb={3} borderColor="transparent">
                                            <Rating name="read-only" className="movie_rating" value={actor.rate} readOnly/>
                                        </Box>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    <Pagination className="pagination">
                        {html_pages}
                    </Pagination>
                    <div className="horizontal_line"></div>
                </main>
                <footer>
                    <p className="footer_question">{feedback}</p>
                    <ButtonGroup aria-label="Basic example">
                        <Button variant="secondary" className="footer_button" disabled={disabled}
                                onClick={(e) => onClickFeedback(e)}
                        >Yes</Button>
                        <Button variant="secondary" className="footer_button"
                                onClick={(e) => onClickFeedback(e)} disabled={disabled}
                        >No</Button>
                    </ButtonGroup>
                    <p></p>
                </footer>
            </>
        );
    }
;
