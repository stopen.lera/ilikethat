// @flow
import React from 'react';
import {GoogleApiWrapper, Map, Marker} from "google-maps-react";

export const MapContainer = (props) => {
    return (
        <div>
            <Map google={props.google}
                 resetBoundsOnResize={true}
                 style={{width: "60%", height: "500px"}}
                 initialCenter={{
                     lat: 50.436739,
                     lng: 30.602006
                 }}
                 zoom={15}
            >
                <Marker
                    position={{
                        lat: 50.436739,
                        lng: 30.602006
                    }}
                />
            </Map>
        </div>
    );
};
export default GoogleApiWrapper({
    apiKey: 'AIzaSyDcHC85A4t3FojWx9gWq634NQJVC89ep9M'
})(MapContainer)
