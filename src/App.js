import React from 'react';
import {Main} from "./Home/Main";
import {AllMovies} from "./Movies/AllMovies";
import {Switch, Route, BrowserRouter} from "react-router-dom"
import {AllActor} from "./Actors/AllActor";
import {TopMovies} from "./Movies/TopMovies";
import {TopActors} from "./Actors/TopActors";
import {MovieStat} from "./Movies/MovieStat";
import {ActorStat} from "./Actors/ActorStat";
import {Contacts} from "./Contacts/Contacts";

export const App = () => {


    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Main}/>
                <Route exact path="/movies" component={AllMovies}/>
                <Route exact path="/actors" component={AllActor}/>
                <Route exact path="/topmovies" component={TopMovies}/>
                <Route exact path="/topactors" component={TopActors}/>
                <Route exact path="/moviestat" component={MovieStat}/>
                <Route exact path="/actorstat" component={ActorStat}/>
                <Route exact path="/contacts" component={Contacts}/>
            </Switch>
        </BrowserRouter>
    );
};
